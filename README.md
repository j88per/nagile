## nAgile - A Node/Mongodb/Vuejs Agile tool/ticket system ##
This project was originally setup as a Laravel backend with a pure VUE frontend.  
It was decided to migrate to NodeJS as the backend and separate the front and 
back ends further by moving to Nodejs.

I have an idea I'd like to develop out here.  The concept is really one of 
replication, and it's viable when using NoSQL vs MySQL.  Similar to how Git is a 
distributed environment, with a local and remote repo, I like the idea of local 
vs remote Agile systems where we can all but eliminate the backend and the application
is purely the front-end.  
SQL and NoSQL systems replicate via changesets, and it would be possible to replicate
between client systems fine, but there's no mechanism for going offline really.  
When a replica set is broken, the app is generally broken.  

Enter PouchDB/CouchDB and many problems go away with the offline-first nature of
pouchdb.  While my goal here isn't specifically offline-first, it supports it seamlessly.

## workflow/operation ##
You start using nAgile as a single user/entity.  As your team grows and you need 
more users to access the system, it's easy to setup a couchdb or cloudant based couch
db server and replicate to that, thereby enabling more users to connect to the system
and begin working.  

Working in offline-agile is the same as any other agile platform.  You log in, 
select a project, and you can see issues in various states, new/backlog/in development, etc.
selecting one, you can update it's status/priority, create/update tasks, and add comments.
You can even post updates to slack.

## Current State ##
This is still in a very pre-alpha state as core concepts are being developed out.  
pouchdb-authentication has a few bugs with the release of couchdb 7 so it's not 
setup at this time and authentication is occurring manually via the url.  It's ugly.
I'm still debating how user authentication will occur and considering services 
like firebase.
All in all, creating projects, stories, tasks, and comments all works.  There's a ton
of cleanup and refactoring that needs to happen to make it more crisp in terms of being
an app, but overall it's really close to being a beta app and not an alpha app.


## How to Demo ##
Everything is currently setup via docker.  Running docker-compose build should 
build 2 containers.  There's an optional 3rd container
under conf/couchDb you can build manually if you want to test sync.  Build and 
run that container and add an external_links: - couchdb to the docker-compose file.
Eventually Electron windows & mac versions will be generated and installable.

Testing