// instantiate vue
window.Vue = require('vue');
import VueRouter from 'vue-router';
import {routes} from '@/router.js';
import lodash from 'lodash';
import App from './app.vue';

// setup an auth trigger
if (localStorage.getItem('Auth') === null) {
    localStorage.setItem('Auth', false);
}

if (localStorage.getItem("user") === null) {
    var user = {};
    user.name = '';
    user.password = '';
    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('Auth', false);
}


// setup store after db
import store from '@/store.js';

// instantiate Vue
import moment from 'moment';
window.moment = moment;

// router
Vue.use(VueRouter);
let router = new VueRouter({
    hashbang: false, mode: 'history', routes
});

const EventBus = new Vue();
Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function() {
            return EventBus;
        }
    }
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        var user = JSON.parse(localStorage.getItem('user'));
        if (!user.name) {
            next({
                path: '/login',
                query: {
                    redirect: to.fullPath,
                },
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use( CKEditor );

import people from '@/database/people.js';
import projects from '@/database/projects.js';
import stories from '@/database/stories.js';

// css/sass imports
import './assets/sass/app.scss';
import './assets/css/app.css';

// main
new Vue({
    el: '#app',
    template: '<App/>',
    components: {App},
    router,
    store,
    beforeCreate() {
        if(localStorage.getItem('project')) {
            this.$store.dispatch('SelectProject', JSON.parse(localStorage.getItem('project')));
        }
	},
    render: h => h(App)
});
