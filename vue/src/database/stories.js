import Vue from 'vue';
import PouchDB from 'pouchdb';
import PouchFind from 'pouchdb-find';
import PouchLiveFind from 'pouchdb-live-find';
PouchDB.plugin(PouchFind);
PouchDB.plugin(PouchLiveFind);

// setup localdb
var localStories = new PouchDB('localStories');
var isAuth = localStorage.getItem('Auth');

// create indexes
localStories.createIndex({
    index: {fields: ['project', 'status', 'priority', 'owner']}
}).catch(function (err) {
    console.log(err);
});

if ((isAuth) && (process.env.REMOTE_ENABLE)) {
    var user = JSON.parse(localStorage.getItem('user'));
    var storiesDB = new PouchDB('http://'+process.env.REMOTE_URL+':'+process.env.REMOTE_PORT+'/stories', {skip_setup: true, auth: { username: user.email, password: user.password }});
    
    localStories.sync(storiesDB, {
        live: true,
        retry: true
    }).on('complete', function () {
        console.log('story sync complete');
        // somehow need to update story list automatically
    }).on('error', function (err) {
        console.log(err);
    }).on('change', function (info) {
        localStorage.setItem('resync', true);
    });
}

// export
Vue.prototype.$localStories = localStories;
