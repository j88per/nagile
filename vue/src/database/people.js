import Vue from 'vue';
import PouchDB from 'pouchdb';
import PouchFind from 'pouchdb-find';
import PouchLiveFind from 'pouchdb-live-find';
PouchDB.plugin(PouchFind);
PouchDB.plugin(PouchLiveFind);

// create dbs
var localPeople = new PouchDB('localPeople');

var isAuth = localStorage.getItem('Auth');

// create indexes
localPeople.createIndex({
    index: {fields: ['email', 'name']}
}).catch(function (err) {
    console.log(err);
});

// only attempt to sync if we have a valid auth.  I kinda want to do a check if remote is enabled
if ((isAuth) && (process.env.REMOTE_ENABLE)) {
    var user = JSON.parse(localStorage.getItem('user'));
    var pouchOpts = { skip_setup: true, auth: { username: user.email, password: user.password } };
    
    var peopleDB = new PouchDB('http://' + process.env.REMOTE_URL + ':' + process.env.REMOTE_PORT + '/people', pouchOpts);

    localPeople.sync(peopleDB, {
        live: true, retry: true
    }).on('complete', function () {
        // sync complete
    }).on('error', function (err) {
        console.log(err);
    });
}

Vue.prototype.$people = localPeople;
