import Vue from 'vue';
import PouchDB from 'pouchdb';

var localProjects = new PouchDB('localProjects');
var isAuth = localStorage.getItem('Auth');

if ((isAuth) && (process.env.REMOTE_ENABLE)) {
    var user = JSON.parse(localStorage.getItem('user'));
    var pouchOpts = { skip_setup: true, auth: { username: user.email, password: user.password }};

    // create dbs
    var projectsDB = new PouchDB('http://'+process.env.REMOTE_URL+':'+process.env.REMOTE_PORT+'/projects', pouchOpts);
    
    localProjects.sync(projectsDB, {live: true, retry: true})
        .on('complete', function () {
        // sync complete
        })
        .on('error', function (err) {
            console.log(err);
        });
}

Vue.prototype.$projects = localProjects;
