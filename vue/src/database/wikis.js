import Vue from 'vue';
import PouchDB from 'pouchdb';
import PouchFind from 'pouchdb-find';
import PouchLiveFind from 'pouchdb-live-find';
PouchDB.plugin(PouchFind);
PouchDB.plugin(PouchLiveFind);

// setup localdb
// setup localdb
var localWiki = new PouchDB('localWiki');
var isAuth = localStorage.getItem('Auth');

// create indexes
localWiki.createIndex({
    index: {fields: ['project', 'owner']}
}).catch(function (err) {
    console.log(err);
});

if ((isAuth) && (process.env.REMOTE_ENABLE)) {
    var user = JSON.parse(localStorage.getItem('user'));
    var wikiDB = new PouchDB('http://'+process.env.REMOTE_URL+':'+process.env.REMOTE_PORT+'/wikis', {skip_setup: true, auth: { username: user.email, password: user.password }});
    
    localWiki.sync(wikiDB, {
        live: true
    }).on('complete', function () {
        console.log('Wiki sync complete');
    }).on('error', function (err) {
        console.log(err);
    }).on('change', function (info) {
        localWiki.setItem('resync', true);
    });
}

// export
Vue.prototype.$localWiki = localWiki;
