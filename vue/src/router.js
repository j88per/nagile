import Vue from 'vue';
import Router from 'vue-router';

// route components
import About from './views/about.vue';
import Sprints from './views/sprints.vue';
import Login from './views/auth/login.vue';
import Logout from './views/auth/logout.vue';

// projects
import Projects from './views/projects/index.vue';
import NewProject from '@/components/projects/create.vue';

// stories
import stories from './views/stories/index.vue';
import storylist from '@/components/stories/list.vue';
import newstory from '@/components/stories/create.vue';
import showstory from '@/components/stories/show.vue';
import storyconflict from '@/components/stories/conflict.vue';
import UserStories from '@/components/stories/userstories.vue';
import Conflicts from '@/views/conflicts/index.vue';
import Archived from '@/components/stories/archived.vue';

// tasks simplified view
import Tasks from './views/tasks/index.vue';

// wikis
import Wiki from './views/wiki/index.vue';
import WikiCreate from '@/components/wikis/create.vue';
import WikiEdit from '@/components/wikis/edit.vue';

// people/teams
import Team from './views/team/members.vue';
import CreatePerson from '@/components/people/create.vue';
import EditUser from '@/components/people/profile.vue';

// reports
import Reports from './views/reports/index.vue';

export const routes = [
    {
        path: '/',
        name: 'Home'
    }, {
        path: '/login',
        name: 'Login',
        component: Login
    }, {
        path: '/logout',
        name: 'Logout',
        component: Logout
    }, {
        path: '/about',
        name: 'About',
        component: About
    }, {
        path: '/team',
        name: 'Team',
        component: Team,
        meta: {
            requiresAuth: true,
        }
    }, {
        path: '/team/create',
        name: 'CreatePerson',
        component: CreatePerson,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/profile', 
        name: 'EditUser',
        component: EditUser,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/projects',
        name: 'Projects',
        component: Projects,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/projects/create',
        name: 'New Project',
        component: NewProject,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/stories/list',
        name: 'list',
        component: storylist,
        meta: {
            requiresAuth: true,
        }
    }, {
        path: '/stories/view/:id',
        name: 'showstory',
        component: showstory,
        props: true,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/stories/create',
        name: 'newstory',
        component: newstory,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/stories/conflicts',
        name: 'conflicts',
        component: Conflicts,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/stories/archived',
        name: 'archived',
        component: Archived,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/stories/conflict/:id',
        name: 'storyconflict',
        component: storyconflict,
        props: true,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/stories/byUser',
        name: 'UserStories',
        component: UserStories,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/wiki', 
        name: 'Wiki',
        component: Wiki
    }, {
        path: '/wiki/create',
        name: 'newwiki',
        component: WikiCreate,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/wiki/edit/:id',
        name: 'editwiki',
        component: WikiEdit,
        meta: {
            requiresAuth: true
        }
    }, {
        path: '/tasks', 
        name: 'Tasks',
        component: Tasks
    }, {
        path: '/reports',
        name: 'reports',
        component: Reports,
        meta: {
            requiresAuth: true
        }
    }
];