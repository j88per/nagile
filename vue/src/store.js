/**
 * Vuex Data Store
 * 
 * this isn't really used the right way as pouchdb and localStorage seem to be 
 * more mainstream.
 * 
 * @package nAgile
 * @author Andrew Carlson <andrew.carlson@here.com>
 */
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

/**
 * The store
 * 
 * @type Vuex.Store
 */
const store = new Vuex.Store({
    state: {
        priorities: [ 'critical', 'high', 'medium', 'low', 'meh' ],
        statuses: [
            'New',
            'Backlog',
            'In Development',
            'In Design',
            'In Test',
            'Ready For Deploy',
            'Deployed',
            'Archived',
            'Will Not Fix'
        ],
        user: {},
        project: {
            name: ''
        },
        stories: [],
        users: [
            {
                'name': 'Andrew Carlson',
                'key': 'D8QMN63P1'
            }, {
                'name': 'Jamie Westhoven',
                'key': 'D91KCLV7V'
            }, {
                'name': 'Paul MacNaughton',
                'key': 'D92MTGU7R'
            }, {
                'name': "Rob 'Roomba' Harris",
                'key': 'DDGCDFVPU'
            }, {
                'name': "Josh Baker",
                'key': 'DE1QTKQKF'
            }, {
                'name': "Matt Klahn",
                'key': 'D91GHPYHZ'
            }, {
                'name': "Rocco 'Meow' Sucato",
                'key': 'DE05RB0KZ'
            }, {
                'name': "Taylor Gries",
                'key': 'DDYFUMA9F'
            }
        ],
        current: {
            story: {},
            project: {}
        },
        reports: {
            selectedReport: ''
        }
    },
    actions: {
        SelectProject({ commit }, payload) {
            commit('SETPROJECT', payload);
        },
        SetStories({ commit, state }, payload) {
            commit('SETSTORIES', payload);
        },
        SetCurrentStory({ commit }, payload) {
            commit('SET_CURRENT_STORY', payload);
        },
        SetCurrentProject({ commit }, payload) {
            commit('SET_CURRENT_PROJECT', payload);
        },
        SetCurrentReport({ commit }, payload) {
            commit('SET_CURRENT_REPORT', payload);
        }
    },
    mutations: {
        SETPROJECT(state, val) {
            // set project persistence
            localStorage.setItem('project', JSON.stringify(val));
            
            // set state
            state.project = val;
        },
        SETSTORIES(state, val) {
            state.stories = val;
        },
        SET_CURRENT_STORY(state, val) {
            state.current.story = val;
        },
        SET_CURRENT_PROJECT(state, val) {
            state.current.project = val;
        },
        SET_CURRENT_REPORT(state, val) {
            state.reports.selectedReport = val;
        }
    },
    getters: {
        getSelectedProject(state) {
            return state.project.name.toLowerCase();
        },
        getStories(state) {
            return state.stories;
        },
        getSync(state) {
            return state.dbSync;
        },
        getStatuses(state) {
            return state.statuses;
        },
        getPriorities(state) {
            return state.priorities;
        },
        getUsers(state) {
            return state.users;
        }
    },
    modules: {
    }
});

export default store;