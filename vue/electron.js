const {app, BrowserWindow} = require('electron');
let win = null;
var url = require('url');
const path = require('path');

app.on('ready', function() {
    win = new BrowserWindow({width: 1200, height: 800});
    
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'dist/index.html'),
        protocol: 'file:',
        slashes: true
    }));
    
    win.webContents.openDevTools();
    require('./electron_menu.js');
    
    
    win.on('closed', function() {
        win = null;
    });
    
    app.on('activate', () => {
        if (win === null) {
            createWindow();
        }
    });
    
    app.on('window-all-closed', function() {
        if (process.platform != 'darwin') {
            app.quit();
        }
    });
})