## Architecture ##

#DB#
Database architecture is fairly simple.  NoSQL databases like MongoDB are considered documents or document storage.  But most people think of them as key|value pair storage.  Neither is wrong. What people are used to is relational database design.  NoSQL is specifically non-relational.  Even if it can handle relationships, the point is to design your data with the fewest relationships as possible. In this case, you should not normalize your data.  
Think of your data as an object, not a bunch of objects related to each other.  Take an ecommerce system with orders.  In relational terms, you would have objects like the customer, addresses, and items all related as an invoice.  In a non-relational database, you would have an order object that contains all of the data needed to fulfill the order, the customer, their address, the shipping address, and items to ship.  
For Agile, a story would be our core object, with an owner, an assignee, any sub-tasks, attached files by reference, and commennts as well as status, priority, etc.  NoSQL database design focuses on speed, the more complete data you deliver, the faster the experience.  SQL Relationships are like additional queries, for building a system where speed is paramount, making many SQL calls can slow down that experience. 


#Back End#


#Front End#
